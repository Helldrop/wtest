package com.example.wingman.wtest.ui.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

import com.example.wingman.wtest.R;
import com.example.wingman.wtest.ui.fragments.HeaderFragment;
import com.example.wingman.wtest.ui.fragments.PatternFragment;
import com.example.wingman.wtest.ui.fragments.PostalFragment;
import com.example.wingman.wtest.ui.fragments.WebviewFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    //JR - The fragments aren't local to allow future devs to send or retrieve data from them
    private PostalFragment postalFragment;
    private HeaderFragment headerFragment;
    private PatternFragment patternFragment;
    private WebviewFragment webviewFragment;

    private MenuItem prevMenuItem;

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupViewPager(viewPager);

        // JR - made the bottomNavigationView coordinate with the viewPager
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_postal:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.action_header:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.action_pattern:
                                viewPager.setCurrentItem(2);
                                break;
                            case R.id.action_webview:
                                viewPager.setCurrentItem(3);
                                break;
                        }

                        return true;
                    }
                });

        // JR - overrided the pageChangeListener to update the selected menu item on bottomNavigationView
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }

                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    // JR - used to hide the keyboard for exercise 3
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    private void setupViewPager(ViewPager viewPager) {
        // JR - Created a viewPager to hold the fragments
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        postalFragment = new PostalFragment();
        headerFragment = new HeaderFragment();
        patternFragment = new PatternFragment();
        webviewFragment = new WebviewFragment();

        adapter.addFragment(postalFragment);
        adapter.addFragment(headerFragment);
        adapter.addFragment(patternFragment);
        adapter.addFragment(webviewFragment);
        viewPager.setAdapter(adapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }
}

