package com.example.wingman.wtest.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.Normalizer;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

@Entity(tableName = "postal_codes", primaryKeys = {"code", "location"})
public class PostalCode {

    private static final String J_COD_POSTAL = "cod_postal";
    private static final String J_EXTENSAO_COD_POSTAL = "extensao_cod_postal";
    private static final String J_LOCALIDADE = "localidade";

    @NonNull
    private String code;

    @NonNull
    private String location;

    // JR - added for search
    @ColumnInfo(name = "full_code")
    private String fullPostalCode;

    public PostalCode(@NonNull String code, @NonNull String location, String fullPostalCode) {
        this.code = code;
        this.location = location;
        this.fullPostalCode = fullPostalCode;
    }

    public PostalCode(JSONObject jsonIn) {

        String codPostal = "";
        String extCodPost = "";
        String local = "";

        try {
            if (jsonIn.has(J_COD_POSTAL)) codPostal = jsonIn.getString(J_COD_POSTAL);
            if (jsonIn.has(J_EXTENSAO_COD_POSTAL)) extCodPost = jsonIn.getString(J_EXTENSAO_COD_POSTAL);
            if (jsonIn.has(J_LOCALIDADE)) local = jsonIn.getString(J_LOCALIDADE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.code = codPostal + "-" + extCodPost;
        this.location = local;

        // JR - used for the search. Removed accents.
        String fullName = (this.code + ", " + this.location);
        fullName = Normalizer.normalize(fullName, Normalizer.Form.NFD);
        fullName = fullName.replaceAll("[^\\p{ASCII}]", "");

        this.fullPostalCode = fullName;
    }

    @NonNull
    public String getCode() {
        return code;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    @NonNull
    public String getLocation() {
        return location;
    }

    public void setLocation(@NonNull String location) {
        this.location = location;
    }

    public String getFullPostalCode() {
        return fullPostalCode;
    }

    public void setFullPostalCode(String fullPostalCode) {
        this.fullPostalCode = fullPostalCode;
    }

}
