package com.example.wingman.wtest.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wingman.wtest.R;
import com.example.wingman.wtest.model.AdapterHeader;
import com.example.wingman.wtest.ui.adapters.HeaderRecyclerAdapter;

import java.util.ArrayList;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class HeaderFragment extends Fragment {

    private RecyclerView headerRecycler;
    // JR - used a fake AppBar / Toolbar just to show the effect. To change the real AppBar I
    // would override the AppBar with a Toolbar and use that.
    // The fake is only to not mess up the rest of the app, and keep it on the respective fragment.
    private RelativeLayout fakeAppBar;
    private TextView fakeTitle;

    private float alpha = 1f; // JR - used to implement incremental changes on the fake appbar alpha.

    public HeaderFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_header, container, false);

        headerRecycler = view.findViewById(R.id.header_recycler_view);
        fakeAppBar = view.findViewById(R.id.header_fake_appbar);
        fakeTitle = view.findViewById(R.id.header_fake_title);

        setHeaderRecycler();

        return view;
    }

    private void setHeaderRecycler() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        headerRecycler.setLayoutManager(mLinearLayoutManager);

        ArrayList<AdapterHeader> items = new ArrayList<>();
        items.add(new AdapterHeader()); // JR - this will be the header

        for (int i = 0; i < 50; i++) {
            AdapterHeader item = new AdapterHeader(getString(R.string.item_slash) + " " + (i + 1));
            items.add(item); // JR - and these are the cells
        }

        HeaderRecyclerAdapter headerRecyclerAdapter = new HeaderRecyclerAdapter(items, getContext());
        headerRecycler.setAdapter(headerRecyclerAdapter);

        headerRecycler.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {

                if (velocityY > 0 && alpha < 1.0f) {
                    alpha += 0.1f;
                    fakeAppBar.setAlpha(alpha);
                } else if (velocityY < 0 && alpha > 0.0f) {
                    alpha -= 0.1f;
                    fakeAppBar.setAlpha(alpha);
                }

                if (alpha <= 0.3f) {
                    fakeTitle.setTextColor(ContextCompat.getColor(getContext(), android.R.color.black));
                } else {
                    fakeTitle.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
                }

                return false;
            }
        });
    }
}