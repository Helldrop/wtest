package com.example.wingman.wtest.model;

/**
 * ** Created by Helldrop on 16/10/2017.
 */

public class AdapterHeader {

    private AdapterType mType;
    private String mTitle;

    public enum AdapterType {
        HEADER,
        CELL
    }

    public AdapterHeader() {
        this.mType = AdapterType.HEADER;
    }

    public AdapterHeader(String mTitle) {
        this.mTitle = mTitle;
        this.mType = AdapterType.CELL;
    }

    public AdapterType getType() {
        return mType;
    }

    public String getTitle() {
        return mTitle;
    }

}
