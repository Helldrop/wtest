package com.example.wingman.wtest.ui.fragments;

import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.wingman.wtest.R;
import com.example.wingman.wtest.db.AppDatabase;
import com.example.wingman.wtest.model.PostalCode;
import com.example.wingman.wtest.ui.adapters.PostalRecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class PostalFragment extends Fragment {

    private static final String MyPrefs = "MY_SHARED_PREFS";
    private static final String IsDataSaved = "IS_DATA_SAVED";

    private EditText editText;
    private RecyclerView postalRecycler;
    private RelativeLayout loadingView;
    private PostalRecyclerAdapter postalRecyclerAdapter;
    private AppDatabase db;
    private ArrayList<PostalCode> postalCodes;

    public PostalFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_postal, container, false);

        // JR - not using a singleton approach because I'm only going to need to access the database here
        db = Room.databaseBuilder(getContext().getApplicationContext(), AppDatabase.class, "database-name").build();

        // JR - used to prevent soft keyboard to hide content
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        editText = view.findViewById(R.id.postal_edit_text);
        postalRecycler = view.findViewById(R.id.postal_recycler_view);
        loadingView = view.findViewById(R.id.postal_container_loading);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // JR - Get all the PostalCode that match the query from SQLite Database via Room
                new DatabaseAsyncQuery().execute(editable.toString());
            }
        });

        // JR - Usually for dealing with webservices I use the Ion Library (also sometimes Retrofit),
        // and I would do something like the call bellow. The problem is, the json has more than 300000 entries
        // without paging... Even after adding largeHeap=true in Manifest, the app would take about 30 minutes
        // to fetch the data and give an OutOfMemory error... To fix this I would do:
        // 1st option: Ask for the webservice to be paged;
        // 2nd option: Try to use the @Streaming function of Retrofit;
        // 3rd option: Try to download the JSON as a File or the CSV file and then parse them.
        // But my laptop is really old ans slow and dealing with large data freezes it, so instead I
        // made a snippet of the JSON response from the site, and saved it on the Assets folder. Then
        // I have an Handler to simulate the fetching delay (for 2 minutes) and parse the data from the snippet.
        // I also added the "2695-650, São João da Talha" entry of the example.

        /*
        Ion.with(getContext())
                .load("http://centraldedados.pt/codigos_postais.json")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // JR - do stuff with the result or error
                        if (result != null) {
                            // JR - parse the data in here
                        }
                    }
                }); */

        // JR - Because we only need to save the data at start, or again if the user closes the app,
        // and since there wasn't more information given, I opted to save the state in sharedPrefs.
        SharedPreferences prefs = getActivity().getSharedPreferences(MyPrefs, MODE_PRIVATE);
        boolean isDataSaved = prefs.getBoolean(IsDataSaved, false);

        if (isDataSaved) {
            setPostalRecycler();
            loadingView.setVisibility(View.GONE);
            postalRecycler.setVisibility(View.VISIBLE);
        } else {
            editText.setEnabled(false);

            int delayTime = 1000 * 60 * 2; // JR - 2 minute of delay to simulate fetching data from HTTP GET
            final Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {

                    String jsonPostalCodes = loadJSONFromAsset();
                    postalCodes = new ArrayList<>();
                    try {
                        JSONArray jsonArray = new JSONArray(jsonPostalCodes);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            // JR - Parse the jsonObjects into PostalCode
                            PostalCode postalCode = new PostalCode(jsonObject);
                            postalCodes.add(postalCode);
                        }

                        // JR - Add all PostalCode to SQLite Database via Room
                        new DatabaseAsyncInsert().execute();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };
            handler.postDelayed(r, delayTime);
        }

        return view;
    }

    private void setPostalRecycler() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        postalRecycler.setLayoutManager(mLinearLayoutManager);

        // JR - Get all the PostalCode from SQLite Database via Room
        new DatabaseAsyncRead().execute();
    }

    private String loadJSONFromAsset() {
        String json;

        try {
            InputStream is = getActivity().getAssets().open("snippet_postal_codes.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }

    private class DatabaseAsyncInsert extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (PostalCode postalCode : postalCodes) {
                db.postalCodeDao().insertAll(postalCode);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            SharedPreferences.Editor editor = getActivity().getSharedPreferences(MyPrefs, MODE_PRIVATE).edit();
            editor.putBoolean(IsDataSaved, true);
            editor.apply();

            setPostalRecycler();
            loadingView.setVisibility(View.GONE);
            postalRecycler.setVisibility(View.VISIBLE);
            editText.setEnabled(true);
        }
    }

    private class DatabaseAsyncRead extends AsyncTask<Void, Void, ArrayList<PostalCode>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<PostalCode> doInBackground(Void... voids) {
            ArrayList<PostalCode> items = new ArrayList<>();
            items.addAll(db.postalCodeDao().getAll());

            return items;
        }

        @Override
        protected void onPostExecute(ArrayList<PostalCode> items) {
            super.onPostExecute(items);

            postalRecyclerAdapter = new PostalRecyclerAdapter(items);
            postalRecycler.setAdapter(postalRecyclerAdapter);
        }
    }

    private class DatabaseAsyncQuery extends AsyncTask<String, Void, ArrayList<PostalCode>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<PostalCode> doInBackground(String... queryArray) {
            ArrayList<PostalCode> items = new ArrayList<>();
            String queryString = queryArray[0];
            String[] splitQueryString = queryString.split(" ");

            if (!queryString.isEmpty()) {
                // JR - Well... I'm really not proud of this solution, but given the time and the problems with Room
                // this is the best workaround I could make under pressure.
                String query1 = splitQueryString.length > 0 ? ("%" + splitQueryString[0].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query2 = splitQueryString.length > 1 ? ("%" + splitQueryString[1].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query3 = splitQueryString.length > 2 ? ("%" + splitQueryString[2].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query4 = splitQueryString.length > 3 ? ("%" + splitQueryString[3].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query5 = splitQueryString.length > 4 ? ("%" + splitQueryString[4].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query6 = splitQueryString.length > 5 ? ("%" + splitQueryString[5].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query7 = splitQueryString.length > 6 ? ("%" + splitQueryString[6].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query8 = splitQueryString.length > 7 ? ("%" + splitQueryString[7].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query9 = splitQueryString.length > 8 ? ("%" + splitQueryString[8].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query10 = splitQueryString.length > 9 ? ("%" + splitQueryString[9].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query11 = splitQueryString.length > 10 ? ("%" + splitQueryString[10].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query12 = splitQueryString.length > 11 ? ("%" + splitQueryString[11].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";
                String query13 = splitQueryString.length > 12 ? ("%" + splitQueryString[12].replaceAll("[^\\p{ASCII}]", "_") + "%") : "%%";

                items.addAll(db.postalCodeDao().getAllByQuery(query1, query2, query3, query4, query5,
                        query6, query7, query8, query9, query10, query11, query12, query13));
            } else {
                items.addAll(db.postalCodeDao().getAll());
            }

            return items;
        }

        @Override
        protected void onPostExecute(ArrayList<PostalCode> items) {
            super.onPostExecute(items);

            postalRecyclerAdapter.setPostalCodes(new ArrayList<PostalCode>());
            postalRecyclerAdapter.setPostalCodes(items);
        }
    }

}


