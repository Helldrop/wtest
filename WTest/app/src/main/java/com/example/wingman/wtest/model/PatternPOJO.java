package com.example.wingman.wtest.model;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class PatternPOJO {

    private String text;
    private String editedText;

    public PatternPOJO(String text) {
        this.text = text;
        this.editedText = "";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEditedText() {
        return editedText;
    }

    public void setEditedText(String editedText) {
        this.editedText = editedText;
    }
}
