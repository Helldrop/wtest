package com.example.wingman.wtest.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.example.wingman.wtest.R;
import com.example.wingman.wtest.model.PatternPOJO;
import com.example.wingman.wtest.ui.adapters.PatternRecyclerAdapter;

import java.util.ArrayList;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class PatternFragment extends Fragment {

    private RecyclerView patternRecycler;

    public PatternFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pattern, container, false);

        // JR - used to prevent soft keyboard to hide content
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        patternRecycler = view.findViewById(R.id.pattern_recycler_view);

        setPatternRecycler();

        return view;
    }

    private void setPatternRecycler() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        patternRecycler.setLayoutManager(mLinearLayoutManager);

        ArrayList<PatternPOJO> items = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            PatternPOJO item = new PatternPOJO(getString(R.string.item_slash) + " " + (i + 1));
            items.add(item);
        }

        PatternRecyclerAdapter patternRecyclerAdapter = new PatternRecyclerAdapter(items);
        patternRecycler.setAdapter(patternRecyclerAdapter);
    }
}
