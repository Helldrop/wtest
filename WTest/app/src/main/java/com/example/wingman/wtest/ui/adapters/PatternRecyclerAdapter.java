package com.example.wingman.wtest.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wingman.wtest.R;
import com.example.wingman.wtest.model.PatternPOJO;

import java.util.ArrayList;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class PatternRecyclerAdapter extends RecyclerView.Adapter<PatternRecyclerAdapter.ViewHolder> {

    private ArrayList<PatternPOJO> mItems;

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mItemTitle;
        private EditText mItemEdit;
        private MyCustomEditTextListener myCustomEditTextListener;

        ViewHolder(View v, MyCustomEditTextListener myCustomEditTextListener) {
            super(v);

            mItemTitle = v.findViewById(R.id.pattern_text_view);
            mItemEdit = v.findViewById(R.id.pattern_edit_text);
            this.myCustomEditTextListener = myCustomEditTextListener;
            mItemEdit.addTextChangedListener(myCustomEditTextListener);
        }
    }

    public PatternRecyclerAdapter(ArrayList<PatternPOJO> items) {
        mItems = items;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_pattern_item_row, parent, false);
        return new ViewHolder(inflatedView, new MyCustomEditTextListener());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final PatternPOJO item = mItems.get(position);

        holder.mItemTitle.setText(item.getText());
        holder.mItemEdit.setText(item.getEditedText());
        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());

        // JR - Position + 3 to set the sequence on 3 instead of 0
        if (((position + 3) % 3) == 0) {
            holder.mItemEdit.setInputType(InputType.TYPE_CLASS_TEXT);
        } else if (((position + 3 - 1) % 3) == 0) {
            holder.mItemEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            holder.mItemEdit.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        }
    }

    //TODO JR - minor bug, not part of the exercise. EditText doesn't hold the value when recycled.
    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mItems.get(position).setEditedText(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}

