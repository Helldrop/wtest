package com.example.wingman.wtest.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.wingman.wtest.model.PostalCode;

import java.util.List;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

@Dao
public interface PostalCodeDao {

    @Query("SELECT * FROM postal_codes")
    List<PostalCode> getAll();

    // JR - I hate my solution but it's my fault for using Room. I've just found out that you cant custom
    // make a Query String, you can only pass arguments. Furthermore, this SQLite doesn't have REGEXP,
    // and the LIKE statement only looks forward %foo%%bar% only matches "foo <some> bar" and not
    // "bar <some> foo". For a proper answer I would append "AND full_code LIKE '%<query_text>%'" for
    // every word found in the original query. Unfortunately Room doesn't provide means to do so, and
    // it's too late for trying something else.
    @Query("SELECT * FROM postal_codes WHERE full_code LIKE :query1 AND "
            + "full_code LIKE :query2 AND full_code LIKE :query3 AND "
            + "full_code LIKE :query4 AND full_code LIKE :query5 AND "
            + "full_code LIKE :query6 AND full_code LIKE :query7 AND "
            + "full_code LIKE :query8 AND full_code LIKE :query9 AND "
            + "full_code LIKE :query10 AND full_code LIKE :query11 AND "
            + "full_code LIKE :query12 AND full_code LIKE :query13")
    List<PostalCode> getAllByQuery(String query1, String query2, String query3, String query4,
                                   String query5, String query6, String query7, String query8,
                                   String query9, String query10, String query11, String query12,
                                   String query13);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(PostalCode... postalCodes);

    @Delete
    void delete(PostalCode postalCode);
}
