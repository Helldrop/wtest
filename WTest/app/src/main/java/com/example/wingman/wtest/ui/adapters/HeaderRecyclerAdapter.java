package com.example.wingman.wtest.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.wingman.wtest.R;
import com.example.wingman.wtest.model.AdapterHeader;

import java.util.ArrayList;

/**
 * ** Created by Helldrop on 16/10/2017.
 */

public class HeaderRecyclerAdapter extends RecyclerView.Adapter<HeaderRecyclerAdapter.ViewHolder> {

    private static final int HEADER = 0;
    private static final int CELL = 1;

    private ArrayList<AdapterHeader> mItems;
    private Context mContext;

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mItemTitle;
        private ImageView mItemImage;

        ViewHolder(View v) {
            super(v);

            mItemTitle = v.findViewById(R.id.header_item_text);
            mItemImage = v.findViewById(R.id.header_item_img);
        }
    }

    public HeaderRecyclerAdapter(ArrayList<AdapterHeader> items, Context context) {
        mItems = items;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {

        AdapterHeader item = mItems.get(position);

        if (item.getType() == AdapterHeader.AdapterType.HEADER) {
            return HEADER;
        }

        return CELL;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == HEADER) {
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_header_header_item_row, parent, false);
            return new ViewHolder(inflatedView);
        } else {
            View inflatedView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_header_cell_item_row, parent, false);
            return new ViewHolder(inflatedView);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final AdapterHeader item = mItems.get(position);

        if (item.getType() == AdapterHeader.AdapterType.HEADER) {
            Glide.with(mContext).load("http://fotos.sapo.pt/zAdUOatho6MYDmUr26Du/").into(holder.mItemImage);
        } else {
            holder.mItemTitle.setText(item.getTitle());
        }
    }
}


