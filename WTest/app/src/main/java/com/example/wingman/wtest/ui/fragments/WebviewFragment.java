package com.example.wingman.wtest.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.wingman.wtest.R;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class WebviewFragment extends Fragment {

    public WebviewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview, container, false);

        WebView webView = view.findViewById(R.id.webview);
        webView.loadUrl(getString(R.string.website_url)); // JR - The URL value is defined on ProductFlavor in gradle config

        return view;
    }
}
