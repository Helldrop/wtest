package com.example.wingman.wtest.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wingman.wtest.R;
import com.example.wingman.wtest.model.PostalCode;

import java.util.ArrayList;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

public class PostalRecyclerAdapter extends RecyclerView.Adapter<PostalRecyclerAdapter.ViewHolder> {

    private ArrayList<PostalCode> mItems;

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mItemText;

        ViewHolder(View v) {
            super(v);

            mItemText = v.findViewById(R.id.postal_item_text);
        }
    }

    public PostalRecyclerAdapter(ArrayList<PostalCode> items) {
        mItems = items;
    }

    public void setPostalCodes(ArrayList<PostalCode> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_postal_item_row, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final PostalCode item = mItems.get(position);

        String fullPostalCode = item.getCode() + ", " + item.getLocation();
        holder.mItemText.setText(fullPostalCode);
    }

}
