package com.example.wingman.wtest.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.wingman.wtest.model.PostalCode;

/**
 * ** Created by Helldrop on 15/10/2017.
 */

@Database(entities = {PostalCode.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PostalCodeDao postalCodeDao();

}
